import json
import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_weather_data(city, state):
    params = {
        "q": f"{city},{state},US",
        "limit": 1,
        "appid": "d208c371240205f2e8af2ed190dd73e0",
    }
    url = "http://api.openweathermap.org/geo/1.0/direct"
    response = requests.get(url, params=params)
    content = json.loads(response.content)

    try:
        latitude = content[0]["lat"]
        longitude = content[0]["lon"]
    except (KeyError, IndexError):
        return None

    params = {
        "lat": latitude,
        "lon": longitude,
        "appid": "d208c371240205f2e8af2ed190dd73e0",
        "units": "imperial",
    }
    url = "https://api.openweathermap.org/data/2.5/weather"
    response = requests.get(url, params=params)
    content = json.loads(response.content)

    try:
        return {
            "description": content["weather"][0]["description"],
            "temp": content["main"]["temp"],
        }
    except (KeyError, IndexError):
        return None


def get_photo(city, state):

    url = "https://api.pexels.com/v1/"
    headers = {"Authorization": "zq4DUC3DSE9KqjKtv7foyCqgLARfhgTm1PEoYpIrqah4jE1eFV7hd1Bm"}
    params = {
        "per_page": 1,
        "query": city + " " + state,
    }
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}



# def get_photo(city, state):
#     url = "https://api.pexels.com/v1/search"
#     headers = {
#         "Authorization": PEXELS_API_KEY
#     }

#     params = {
#         "per_page": 1,
#         "query": city + " " + state
#     }

#     response = requests.get(url, params=params, headers=headers)
#     content = json.loads(response.content)

#     return {"picture_url": content["photos"][0]["src"]["original"]}


# def get_weather_data(city, state):
#     # Update the geocoding API URL with the correct endpoint
#     geocoding_url = f"https://api.example.com/geocode?city={city}&state={state}"
#     geocoding_response = requests.get(geocoding_url)
#     geocoding_data = geocoding_response.json()

#     latitude = geocoding_data["latitude"]
#     longitude = geocoding_data["longitude"]

#     weather_url = f"https://api.openweathermap.org/data/2.5/weather?lat={latitude}&lon={longitude}&appid={OPEN_WEATHER_API_KEY}"
#     weather_response = requests.get(weather_url)
#     weather_data = weather_response.json()

#     if "main" in weather_data and "weather" in weather_data:
#         temperature = weather_data["main"]["temp"]
#         description = weather_data["weather"][0]["description"]

#         return {
#             "temperature": temperature,
#             "description": description
#         }
#     else:
#         return {
#             "temperature": None,
#             "description": None
#         }
