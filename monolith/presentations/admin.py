from django.contrib import admin

from .models import Presentation, Status


@admin.register(Presentation)
class PresentationAdmin(admin.ModelAdmin):
    list_display = (
            "presenter_name",
            "id",
       )


@admin.register(Status)
class StatusAdmin(admin.ModelAdmin):
    list_display = (
            "id",
       )
